package posPackage;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OverrideProperties {

	@SerializedName("sisal.environment")
	@Expose
	private String sisalEnvironment;

	public String getSisalEnvironment() {
		return sisalEnvironment;
	}

	public void setSisalEnvironment(String sisalEnvironment) {
		this.sisalEnvironment = sisalEnvironment;
	}

}