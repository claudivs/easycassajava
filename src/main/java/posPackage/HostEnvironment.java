package posPackage;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HostEnvironment {

	@SerializedName("envlist")
	@Expose
	private List<EnvList> envlist = null;

	public List<EnvList> getEnvlist() {
		return envlist;
	}

	public void setEnvlist(List<EnvList> envlist) {
		this.envlist = envlist;
	}

}