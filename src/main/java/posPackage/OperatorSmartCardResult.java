package posPackage;

public class OperatorSmartCardResult {
	
	public enum Result {
        OK("SLE442 card read correctly"),
        READ_ERROR("SLE442 card read error");

		private final String description;

		Result(final String description) {
			this.description = description;
		}

		@Override
		public String toString() {
			return this.description;
		}    
	}

    private Result result;
    private String pin;
    private String endValidityDate;

	public void setResult(Result result) {
		this.result = result;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

    public void setEndValidityDate(String endValidityDate) {
        this.endValidityDate = endValidityDate;
    }

	public Result getResult() {
		return this.result;
	}

	public String getPin() {
		return this.pin;
	}

    public String getEndValidityDate() {
        return this.endValidityDate;
    }


}
