package posPackage;

public class InitResult {

	public enum Result {
		SUCCESS("POS device Init sucess"), 
		FIRMWARE_UPDATE_NEEDED("Firmware outdated. Update needed"), 
		RSA_KEY_FAILURE("POS device RSA key configuration failure"), 
		CB2_CONF_FAILURE("POS device CB2 configuration failure"), 
		GENERIC_ERROR("POS device initialization generic error");

		private final String description;

		Result(final String description) {
			this.description = description;
		}

		@Override
		public String toString() {
			return this.description;
		}
	}

	private Result result;
	private String[] receipt;

	public void setResult(Result result) {
		this.result = result;
	}

	public Result getResult() {
		return this.result;
	}

	public void setReceipt(String[] receipt) {
		this.receipt = receipt;
	}

	public String[] getReceipt() {
		return receipt;
	}
}
