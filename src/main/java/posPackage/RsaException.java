package posPackage;

public class RsaException extends Exception {

	private static final long serialVersionUID = 5554626002820623780L;
	
	public RsaException() {
	}
	
	public RsaException(Exception e) {
		super(e);
	}
	
	public RsaException(String e) {
		super(e);
	}

}
