package posPackage;

public enum DeviceType {  
	BIG_TOUCH("bigtouch"),
	FUSION("fusion"),
	WAVE("wave");
	
	private String value;

	private DeviceType(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}