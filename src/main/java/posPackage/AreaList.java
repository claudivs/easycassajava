package posPackage;

import java.util.List;

public class AreaList {

	private String area;
	
	private List<AddressList> addresslist = null;

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public List<AddressList> getAddresslist() {
		return addresslist;
	}

	public void setAddresslist(List<AddressList> addresslist) {
		this.addresslist = addresslist;
	}

}