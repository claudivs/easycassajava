package posPackage;

public enum CardType {
	DEBIT_CARD("Debit card"),
	CREDIT_CARD("Credit card");

	private final String description;

	CardType(final String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return this.description;
	}    
}

