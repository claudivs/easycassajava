package posPackage;

public class LastElectronicPaymentReceiptResult {

    public enum Result {
        OK("Last electronic payment receipt data retrieved correctly"),
        MAXIMUM_REPRINTS_REACHED("Weekly maximum number of reprints reached"),
        ERROR("Last electronic payment receipt data error");
     
        private final String description;

        Result(final String description) {
            this.description = description;
        }

        @Override
        public String toString() {
            return this.description;
        }    
     }
         
     private Result result;
     private String receipt[];
         
     public void setResult(Result result) {
		this.result = result;
	}

	public void setReceipt(String[] receipt) {
		this.receipt = receipt;
	}

	public Result getResult() {
         return this.result;
     }

     public String[] getReceipt() {
         return this.receipt;
     }    


}
