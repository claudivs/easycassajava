package posPackage;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EnvList {

	@SerializedName("environment")
	@Expose
	private String environment;
	@SerializedName("arealist")
	@Expose
	private List<AreaList> arealist = null;

	public String getEnvironment() {
		return environment;
	}

	public void setEnvironment(String environment) {
		this.environment = environment;
	}

	public List<AreaList> getArealist() {
		return arealist;
	}

	public void setArealist(List<AreaList> arealist) {
		this.arealist = arealist;
	}

}