package posPackage;

public class Cb2ConfException extends Exception {

	private static final long serialVersionUID = -8435642941836590648L;

	public Cb2ConfException() {
	}

	public Cb2ConfException(Exception e) {
		super(e);
	}
	
	public Cb2ConfException(String e) {
		super(e);
	}

}
