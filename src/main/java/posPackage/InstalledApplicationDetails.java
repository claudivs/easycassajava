
package posPackage;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class InstalledApplicationDetails {

    @SerializedName("GlobalVersion")
    @Expose
    private String globalVersion;
    @SerializedName("PackageList")
    @Expose
    private List<PackageList> packageList = null;

    public String getGlobalVersion() {
        return globalVersion;
    }

    public void setGlobalVersion(String globalVersion) {
        this.globalVersion = globalVersion;
    }

    public List<PackageList> getPackageList() {
        return packageList;
    }

    public void setPackageList(List<PackageList> packageList) {
        this.packageList = packageList;
    }

}
