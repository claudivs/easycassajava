package posPackage;

public class UpdateFirmwareException extends Exception {

	private static final long serialVersionUID = -7511646556266378931L;

	public UpdateFirmwareException() {
	}

	public UpdateFirmwareException(Exception e) {
		super(e);
	}
	
	public UpdateFirmwareException(String e) {
		super(e);
	}

}
