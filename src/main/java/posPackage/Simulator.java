package posPackage;

import java.util.concurrent.TimeUnit;

import posPackage.FiscalCodeResult.AcquisitionMode;

/**
 * Class to be used for POS STUB MODE
 * @author sisal
 *
 */
class Simulator {

	public static InitResult init() {
		InitResult initResult = new InitResult();
		initResult.setResult(InitResult.Result.SUCCESS);
		return initResult;
	}
	public static DeviceStatusResult deviceStatus() {
		DeviceStatusResult deviceStatusResult = new DeviceStatusResult();
		deviceStatusResult.setResult(DeviceStatusResult.Result.OK);
		return deviceStatusResult;
	}

	public static ElectronicPaymentResult electronicPayment(CardType cardType, int amount, long transactionId,
			TransactionType transactionType) {
		ElectronicPaymentResult electronicPaymentResult = new ElectronicPaymentResult();
		electronicPaymentResult.setResult(ElectronicPaymentResult.Result.OK);
		electronicPaymentResult.setDateTime(Long.toString(System.currentTimeMillis()));
		electronicPaymentResult.setResultCode("P00");
		String[] lines = new String[22];
		lines[0] =  "          Visa          ";
		lines[1] =  " ";
		lines[2] =  "        ACQUISTO        ";
		lines[3] =  "   SISALPAY - GE4046    ";
		lines[4] =  "     VIA CRISPI,99      ";
		lines[5] =  " ";
		lines[6] =  "Eserc.      101010110001";
		lines[7] =  "A.I.I.C.     88105000003";
		lines[8] =  "Data 23/06/15  Ora 10:28";
		lines[9] =  "TML 00000243 STAN 000005";
		lines[10] = "Mod.  Online    B.C. MAG";
		lines[11] = "AUT. 692589             ";
		lines[12] = "PAN     455777******1113";
		lines[13] = "SCAD                ****";
		lines[14] = " ";
		lines[15] = " ";
		lines[16] = "IMPORTO EUR         1,00";
		lines[17] = " ";
		lines[18] = "     TRANSAZIONE OK     ";
		lines[19] = " ";
		lines[20] = "  ARRIVEDERCI E GRAZIE  ";
		lines[21] = " ";
		
		electronicPaymentResult.setAddInfo("BNLCCTCK");
		electronicPaymentResult.setCodAuth("692589");
		electronicPaymentResult.setDateTime("1741028");
		electronicPaymentResult.setAcquirer("88105000003");
		
		electronicPaymentResult.setReceipt(lines);
		return electronicPaymentResult;
	}

	public static SecuredCardDataResult readSecuredCardData(String panMask) {
		SecuredCardDataResult cardDataResult = new SecuredCardDataResult();
		cardDataResult.setResult(SecuredCardDataResult.Result.OK);
		cardDataResult.setMaskedPan("40**********0753");
		cardDataResult.setEncryptedPan("0cd3b4818db4ce44695654f5d30164f984c47a3021ae571163320afd4822b14d9a2fad34106a9e6e8c24208a38e1fdbb23031cd8d66e72b74dd0fe82af468d20889e081c2bf8d0775d66c8c935f7adf613515ac637512cee01b0dec196cfb836cd13033d28d6eb9f2e32f8e31c1c575272ae46c3083a5b0d1581309da00edbe3175795eef6f130e3c40346a459dc4ff7d2c8ccfb2fc70014c514cfda9ddfbdc5d5118708e77eb3f03aba363e41283d7f7a1cc781260bbe5eaab6a455b5a29d22430d6d9e19c78116c402c1298b4b7b2f38a6ab966a4ad32739831cddee43b5f28ec9b6766405d2065e79d04b58f1002f278cffbebfad71f6b9e6c0a579027619");
	
		return cardDataResult;
	}

    public static OperatorSmartCardResult readOperatorSmartCard() {
        // simulate a delay in reading operator's smart card
        // This is to to TEST the scenario where the user can CANCEL the read operation before inserting the OperatorSmartCard.
        try {
            TimeUnit.SECONDS.sleep(5);
        }
        catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }

        OperatorSmartCardResult cardResult = new OperatorSmartCardResult();
        cardResult.setResult(OperatorSmartCardResult.Result.OK);
        cardResult.setPin("12345");
        cardResult.setEndValidityDate("1546297199000");
        return cardResult;
    }

	public static FiscalCodeResult readFiscalCode() {
		FiscalCodeResult fiscalCodeResult = new FiscalCodeResult();
		fiscalCodeResult.setAcquisitionMode(AcquisitionMode.CHIP);
		fiscalCodeResult.setBirthDate("01/01/1980");
		fiscalCodeResult.setFiscalCode("RSSMRA80A01H501U");
		fiscalCodeResult.setName("Mario");
		fiscalCodeResult.setSurname("Rossi");
		fiscalCodeResult.setResult(FiscalCodeResult.Result.OK);
		return fiscalCodeResult;
	}

	public static LastElectronicPaymentReceiptResult getLastElectronicPaymentReceipt() {
		LastElectronicPaymentReceiptResult electronicPaymentReceiptResult = new LastElectronicPaymentReceiptResult();
		String[] lines = new String[22];
		lines[0] =  "          Visa          ";
		lines[1] =  " ";
		lines[2] =  "        ACQUISTO        ";
		lines[3] =  "   SISALPAY - GE4046    ";
		lines[4] =  "     VIA CRISPI,99      ";
		lines[5] =  " ";
		lines[6] =  "Eserc.      101010110001";
		lines[7] =  "A.I.I.C.     88105000003";
		lines[8] =  "Data 23/06/15  Ora 10:28";
		lines[9] =  "TML 00000243 STAN 000005";
		lines[10] = "Mod.  Online    B.C. MAG";
		lines[11] = "AUT. 692589             ";
		lines[12] = "PAN     455777******1113";
		lines[13] = "SCAD                ****";
		lines[14] = " ";
		lines[15] = " ";
		lines[16] = "IMPORTO EUR         1,00";
		lines[17] = " ";
		lines[18] = "     TRANSAZIONE OK     ";
		lines[19] = " ";
		lines[20] = "  ARRIVEDERCI E GRAZIE  ";
		lines[21] = " ";
		electronicPaymentReceiptResult.setReceipt(lines);
		electronicPaymentReceiptResult.setResult(LastElectronicPaymentReceiptResult.Result.OK);
		return electronicPaymentReceiptResult;
	}

	public static SoftwareVersionsResult getSoftwareVersions() {
		SoftwareVersionsResult softwareVersionsResult = new SoftwareVersionsResult();
		softwareVersionsResult.setResult(SoftwareVersionsResult.Result.OK);
		
		/*
		 * SYS = Sistema Operativo
			MAN = Manager
			SSA = System Security Application
			MST = Master
			EMV = Payment application
			ECR = Gestione Cassa
			SSL = SSL Manager
			EXT = External Manager
			EDL = ECR DLL
		 */
		
		softwareVersionsResult.setCashManagerVersion("1.0.0"); //ECR
		softwareVersionsResult.setEcrDllVersion("1.0.1"); //EDL
		softwareVersionsResult.setExternalManagerVersion("1.0.2"); //EXT
		softwareVersionsResult.setJavaLibVersion("1.0.3");
		softwareVersionsResult.setManagerVersion("1.0.4"); //MAN
		softwareVersionsResult.setMasterVersion("1.0.5"); //MST
		softwareVersionsResult.setOperatingSystemVersion("1.0.6"); //SYS
		softwareVersionsResult.setPaymentApplicationVersion("1.0.7"); //EMV
		softwareVersionsResult.setSslManagerVersion("1.0.8"); //SSL
		softwareVersionsResult.setSystemSecurityApplicationVersion("1.0.9"); //SSA
		return softwareVersionsResult;
	}

	public static DeviceTestResult deviceTest() {
		DeviceTestResult deviceTestResult = new DeviceTestResult();
		deviceTestResult.setResult(DeviceTestResult.Result.OK);
		return deviceTestResult;
	}

}
