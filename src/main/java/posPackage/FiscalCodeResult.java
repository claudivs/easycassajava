package posPackage;

public class FiscalCodeResult {

	public enum Result {
		OK("Health insurance card read correctly"),
		READ_ERROR("Health insurance card read error");

		private final String description;

		Result(final String description) {
			this.description = description;
		}

		@Override
		public String toString() {
			return this.description;
		}    
	}

	public enum AcquisitionMode {
		MANUAL("Fiscal code manual input", "MAN"),
		MAGNETIC_STRIPE("Fiscal code magnetic stripe input", "MAG"),
		CHIP("Fiscal code chip input", "ICC"),
		ERROR("Error in fiscal code input", "WRONG");

		private final String description;
		private final String paymentType;

		AcquisitionMode(final String description, final String paymentType) {
			this.description = description;
			this.paymentType = paymentType;
		}

		public static AcquisitionMode getEnumByPaymentType(String paymentType) {
			for (AcquisitionMode acqMode : AcquisitionMode.values()) {
				if(acqMode.getPaymentType().equals(paymentType))
					return acqMode;
			}
			return null;
		}
		
		@Override
		public String toString() {
			return this.description;
		}

		public String getPaymentType() {
			return paymentType;
		}
	}

	private Result result;
	private AcquisitionMode acquisitionMode;
	private String fiscalCode;
	private String name;
	private String surname;
	private String birthDate;

	public void setResult(Result result) {
		this.result = result;
	}

	public void setAcquisitionMode(AcquisitionMode acquisitionMode) {
		this.acquisitionMode = acquisitionMode;
	}

	public void setFiscalCode(String fiscalCode) {
		this.fiscalCode = fiscalCode;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public Result getResult() {
		return this.result;
	}

	public AcquisitionMode getAcquisitionMode() {
		return this.acquisitionMode;
	}

	public String getFiscalCode() {
		return this.fiscalCode;
	}

	public String getName() {
		return this.name;
	}

	public String getSurname() {
		return this.surname;
	}

	public String getBirthDate() {
		return this.birthDate;
	}
}

