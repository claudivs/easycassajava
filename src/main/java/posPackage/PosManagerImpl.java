package posPackage;

import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import posPackage.FiscalCodeResult.AcquisitionMode;
import posPackage.PosManagerConstantsAndEnums.PosVersions;
import posPackage.PosManagerConstantsAndEnums.TransactionDataResultCode;
import posPackage.PosManagerConstantsAndEnums.TransactionDataStatus;
import posPackage.ConfigurationManagerService;
import posPackage.SisalConfigurableComponent;

import it.netsw.apps.ftfs.posdealer.impl.ReconciliationData;
import it.netsw.apps.ftfs.posdealer.impl.TransactionData;
import it.netsw.apps.ftfs.sisal.PosSisal;
import it.netsw.coms.device.ingenico.pos.comms.PaymentType;
import it.netsw.coms.device.tcp.TcpDeviceFactory;

public class PosManagerImpl extends SisalConfigurableComponent implements PosManager {

	private Logger log = LoggerFactory.getLogger(getClass());
	//private CommonConfigService commonConfigService;

	/*
	 * protected void activate(ComponentContext componentContext) {
	 * log.debug("activate >>"); update(properties); log.debug("activate <<"); }
	 * 
	 * public void deactivate(ComponentContext componenetContext) {
	 * log.debug("deactivate >> <<"); }
	 */

	public void update(Map<String, Object> properties) {
		log.debug("update >>");
		if (properties != null && !properties.isEmpty()) {
			Iterator<Entry<String, Object>> it = properties.entrySet().iterator();
			while (it.hasNext()) {
				Entry<String, Object> entry = it.next();
				log.debug("New property - " + entry.getKey() + " = " + entry.getValue() + " of type "
						+ entry.getValue().getClass().toString());
			}
		}

		log.debug("update <<");
	}

	@Override
	public synchronized InitResult init() {
		log.info("Call init");
		InitResult result;
		// if (!(boolean) properties.get(MockProperties.INIT_MOCK.getCode())) {
		if (!PosManagerConstantsAndEnums.INIT_MOCK) {
			result = realInit();
		} else {
			log.info("Mock init");
			result = Simulator.init();
		}
		return result;
	}
	
	// TODO
	private int getRiceId()
	{
		return 1;
	}

	private InitResult realInit() {
		InitResult result;
		result = new InitResult();
		try {
			if (isPosHwConnected()) {
				log.info("Getting data");
				PosSisal pos = getPos();
				TransactionData tdata = pos.doStatus();
				if (verifyResultCode(tdata)) {
					log.info("Set of control");
					versionsControl(tdata);
					setPublicRsa(pos);
					log.info("Id Rice: " + getRiceId());
					result.setReceipt(cb2Configuration(pos, tdata, String.valueOf(getRiceId())));
					log.info("Send ok");
					result.setResult(InitResult.Result.SUCCESS);
				} else {
					result.setResult(InitResult.Result.GENERIC_ERROR);
				}
			} else {
				result.setResult(InitResult.Result.SUCCESS);
				log.info("POS not initialized since not available on HW");
			}
		} catch (UpdateFirmwareException e) {
			result.setResult(InitResult.Result.FIRMWARE_UPDATE_NEEDED);
		} catch (RsaException e) {
			result.setResult(InitResult.Result.RSA_KEY_FAILURE);
		} catch (Cb2ConfException e) {
			result.setResult(InitResult.Result.CB2_CONF_FAILURE);
		} catch (Exception e) {
			result.setResult(InitResult.Result.GENERIC_ERROR);
		}
		return result;
	}

	@Override
	public synchronized DeviceStatusResult deviceStatus() {
		log.info("Call deviceStatus");
		log.info("Getting pos status");
		DeviceStatusResult devInterlStatus = new DeviceStatusResult();
		// if (!(boolean) properties.get(MockProperties.DEVICE_STATUS_MOCK.getCode())) {
		if (!PosManagerConstantsAndEnums.DEVICE_STATUS_MOCK) {
			if (isPosHwConnected()) {
				devInterlStatus = realDeviceStatus();
			} else {
				log.warn("deviceStatus >> unable to get data from POS from dev");
				devInterlStatus.setResult(DeviceStatusResult.Result.NOT_CONNECTED);
			}
		} else {
			log.info("Mock device status");
			devInterlStatus = Simulator.deviceStatus();
		}
		log.debug("deviceStatus >> status:{}", devInterlStatus.getResult());
		return devInterlStatus;
	}

	private DeviceStatusResult realDeviceStatus() {
		DeviceStatusResult devInterlStatus;
		devInterlStatus = new DeviceStatusResult();
		try {
			TransactionData tdata = getPos().doStatus();
			if (tdata != null && tdata.getStatus() != null) {
				log.debug("Pos Status: " + tdata.getStatus());
				log.debug("Enum Status: " + TransactionDataStatus.getEnumByCode(tdata.getStatus()));
				switch (TransactionDataStatus.getEnumByCode(tdata.getStatus())) {
				case STATUS_0:
				case STATUS_1:
				case STATUS_3:
				case STATUS_4:
				case STATUS_5:
				case STATUS_6:
				case STATUS_8:
					devInterlStatus.setResult(DeviceStatusResult.Result.PAYMENTS_DISABLED);
					break;
				case STATUS_2:
				case STATUS_7:
					devInterlStatus.setResult(DeviceStatusResult.Result.OK);
					break;
				case STATUS_9:
					devInterlStatus.setResult(DeviceStatusResult.Result.CRITICAL_ERROR);
					break;
				case STATUS_NULL:
				default:
					devInterlStatus.setResult(DeviceStatusResult.Result.NOT_CONNECTED);
					break;
				}
			} else {
				log.warn("deviceStatus >> unable to get data from POS");
				devInterlStatus.setResult(DeviceStatusResult.Result.NOT_CONNECTED);
			}
		} catch (Exception e) {
			log.warn("Exception to init pos");
			log.warn("deviceStatus >> unable to get data from POS");
			devInterlStatus.setResult(DeviceStatusResult.Result.NOT_CONNECTED);
		}
		return devInterlStatus;
	}

	@Override
	public synchronized ElectronicPaymentResult electronicPayment(CardType cardType, String xid, int amount,
			long transactionId, TransactionType transactionType, String ptype) {
		log.info("Call electronicPayment");
		ElectronicPaymentResult electronicPaymentResult;
		// if (!(boolean)
		// properties.get(MockProperties.ELECTRONIC_PAYMENT_MOCK.getCode())) {
		if (!PosManagerConstantsAndEnums.ELECTRONIC_PAYMENT_MOCK) {
			electronicPaymentResult = realElectronicPayment(cardType, xid, amount, transactionId, transactionType,
					ptype);
		} else {
			log.info("Mock electronic payment");
			electronicPaymentResult = Simulator.electronicPayment(cardType, amount, transactionId, transactionType);
		}
		return electronicPaymentResult;
	}

	private ElectronicPaymentResult realElectronicPayment(CardType cardType, String xid, int amount, long transactionId,
			TransactionType transactionType, String ptype) {
		ElectronicPaymentResult electronicPaymentResult;
		electronicPaymentResult = new ElectronicPaymentResult();
		log.debug("cardType: " + cardType.toString());
		log.debug("xid: " + xid);
		log.debug("amount: " + amount);
		log.debug("transactionId: " + transactionId);
		log.debug("transactionType: " + transactionType);
		log.debug("ptype: " + ptype);
		try {
			PaymentType paymentType = selectPaymentType(ptype);
			electronicPaymentResult = electronicPayment(getPos(), cardType, xid, amount, transactionId,
					electronicPaymentResult, paymentType);
		} catch (Exception e) {
			log.error("Exception: " + e);
			for (int i = 0; i < e.getStackTrace().length; i++) {
				if (i >= 120)
					break;
				log.error("Exception Stack: " + e.getStackTrace()[i]);
			}
			electronicPaymentResult.setResult(ElectronicPaymentResult.Result.PAYMENT_ERROR);
		}
		return electronicPaymentResult;
	}

	@Override
	public synchronized SecuredCardDataResult readSecuredCardData(String panMask) {
		log.info("Call Send secure field");
		SecuredCardDataResult result;
		// if (!(boolean)
		// properties.get(MockProperties.READ_SECURED_CARD_DATA_MOCK.getCode())) {
		if (!PosManagerConstantsAndEnums.READ_SECURED_CARD_DATA_MOCK) {
			result = realReadSecuredCardData(panMask);
		} else {
			log.info("Mock read secured card data");
			result = Simulator.readSecuredCardData(panMask);
		}
		return result;
	}

	private SecuredCardDataResult realReadSecuredCardData(String panMask) {
		SecuredCardDataResult result;
		result = new SecuredCardDataResult();
		try {
			log.debug("Cid: " + PosManagerConstantsAndEnums.CB2_CID);
			log.debug("Input lenght: " + panMask.length());
			log.debug("PanMask: " + panMask);
//			log.debug("Conf Type: " + properties.get(ConfigurationBundle.CONF_TYPE.getCode()));
//			log.debug("Timeout: " + properties.get(ConfigurationBundle.TIME_OUT.getCode()));
			log.debug("Conf Type: " + Integer.toString(PosManagerConstantsAndEnums.CONF_TYPE));
			log.debug("Timeout: " + Integer.toString(PosManagerConstantsAndEnums.TIME_OUT));
			String panMaskNew = Utils.fillZeroDx(20, panMask);
			log.debug("PanMask for POS: " + panMaskNew);
			TransactionData data = getPos().doSecureField(PosManagerConstantsAndEnums.CB2_CID, panMask.length(),
					panMaskNew, PosManagerConstantsAndEnums.CONF_TYPE, PosManagerConstantsAndEnums.TIME_OUT);
			log.debug("Pos doSecureField response: " + data.getResultCode());
			if (verifyResultCode(data)) {
				log.debug("EncryptedPan: " + data.getCipheredData());
				result.setEncryptedPan(data.getCipheredData());
				log.debug("MaskedPan: " + data.getMaskedData());
				String maskedPanCorrectValue = data.getMaskedData().replaceAll("-", "*").replaceAll(" ", "");
				log.debug("MaskePan after work: " + maskedPanCorrectValue);
				result.setMaskedPan(maskedPanCorrectValue);
				result.setResult(SecuredCardDataResult.Result.OK);
			} else {
				log.info("Error to mask");
				result.setResult(SecuredCardDataResult.Result.MASK_ERROR);
			}
		} catch (Exception e) {
			log.error("Exception: " + e);
			log.error("Exception message: " + e.getMessage());
			if (e.getStackTrace().length > 3) {
				log.error("Exception Stack: " + e.getStackTrace()[0]);
				log.error("Exception Stack: " + e.getStackTrace()[1]);
				log.error("Exception Stack: " + e.getStackTrace()[2]);
			}
			result.setResult(SecuredCardDataResult.Result.MASK_ERROR);
		}
		return result;
	}

	@Override
	public synchronized OperatorSmartCardResult readOperatorSmartCard() {
		log.info("Call readOperatorSmartCard");
		OperatorSmartCardResult result;
		if (!PosManagerConstantsAndEnums.READ_OPERATOR_SMART_CARD_MOCK) {
			result = realtReadOperatorSmartCard();
		} else {
			log.info("Mock read operator smart card");
			result = Simulator.readOperatorSmartCard();
		}
		return result;
	}

	private OperatorSmartCardResult realtReadOperatorSmartCard() {
		OperatorSmartCardResult result;
		result = new OperatorSmartCardResult();
		try {
			log.info("Send SLE 442");
			log.debug("cid: \"" + PosManagerConstantsAndEnums.CB2_CID + "\"");
			log.debug("wait: \"" + PosManagerConstantsAndEnums.SMART_CARD_WAIT + "\"");
			log.debug("psc type: \"" + PosManagerConstantsAndEnums.SMART_CARD_PSC_TYPE + "\"");
			log.debug("card psc: \"" + PosManagerConstantsAndEnums.SMART_CARD_PSC + "\"");
			log.debug("message: \"" + PosManagerConstantsAndEnums.SMART_CARD_MESSAGE + "\"");
			TransactionData data = getPos().doSLE442(PosManagerConstantsAndEnums.CB2_CID,
					(int) PosManagerConstantsAndEnums.SMART_CARD_WAIT,
					(int) PosManagerConstantsAndEnums.SMART_CARD_PSC_TYPE,
					(String) PosManagerConstantsAndEnums.SMART_CARD_PSC,
					(String) PosManagerConstantsAndEnums.SMART_CARD_MESSAGE);
			if (verifyResultCode(data)) {
				log.info("Card Sle442 data: " + data.getCardData());
				result.setEndValidityDate(Utils.getEndValidityDateFromOperatorCard(data.getCardData()));
				result.setPin(Utils.getPinDateFromOperatorCard(data.getCardData()));
				result.setResult(OperatorSmartCardResult.Result.OK);
			} else {
				result.setResult(OperatorSmartCardResult.Result.READ_ERROR);
			}
		} catch (Exception e) {
			log.error("Exception: " + e);
			result.setResult(OperatorSmartCardResult.Result.READ_ERROR);
		}
		return result;
	}

	@Override
	public synchronized FiscalCodeResult readFiscalCode(int codeType) {
		log.info("Call readFiscalCode");
		FiscalCodeResult result;
		if (!PosManagerConstantsAndEnums.READ_FISCAL_CODE_MOCK) {
			result = verifyAndReadFiscalCode(codeType);
		} else {
			log.info("Mock read fiscal code");
			result = Simulator.readFiscalCode();
		}
		log.debug("Out readFiscalCode");
		return result;
	}

	@Override
	public synchronized LastElectronicPaymentReceiptResult getLastElectronicPaymentReceipt() {
		log.info("Call getLastElectronicPaymentReceipt");
		LastElectronicPaymentReceiptResult result;
		if (!PosManagerConstantsAndEnums.GET_LAST_ELECTRONIC_PAYMENT_RECEIPT_MOCK) {
			result = realGetLastElectronicPaymentReceipt();
		} else {
			log.info("Mock get last electronic payment receipt");
			result = Simulator.getLastElectronicPaymentReceipt();
		}
		return result;
	}

	private LastElectronicPaymentReceiptResult realGetLastElectronicPaymentReceipt() {
		LastElectronicPaymentReceiptResult result;
		result = new LastElectronicPaymentReceiptResult();
		try {
			log.debug(
					"getLastElectrioncPaymentReceipt: " + PosManagerConstantsAndEnums.PAYMENT_ECR);
			PosSisal pos = getPos();
			boolean fiscalResponse = pos.doReprintFinReceipt((boolean) PosManagerConstantsAndEnums.PAYMENT_ECR);
			if (fiscalResponse) {
				String[] receipt = new String[1];
				receipt[0] = pos.getLastTrReceipt();
				result.setReceipt(receipt);
				result.setResult(LastElectronicPaymentReceiptResult.Result.OK);
			} else {
				result.setResult(LastElectronicPaymentReceiptResult.Result.ERROR);
			}
		} catch (Exception e) {
			log.error("Eccezione: ", e);
			result.setResult(LastElectronicPaymentReceiptResult.Result.ERROR);
		}
		return result;
	}

	@Override
	public synchronized SoftwareVersionsResult getSoftwareVersions() {
		log.info("Call getSoftwareVersions");
		SoftwareVersionsResult result;
		if (!(boolean) PosManagerConstantsAndEnums.GET_SOFTWARE_VERSIONS_MOCK) {
			result = new SoftwareVersionsResult();
			try {
				log.info("Software Versions");
				PosSisal pos = getPos();
				result.setJavaLibVersion(pos.dolibVersion());
				String versions = pos.doStatus().getPosVersions();
				log.debug("Getting versions map");
				Map<PosVersions, String> versionsMap = Utils.getPosVersionsMap(versions);
				log.debug("Getted versions map");
				Set<PosVersions> keys = versionsMap.keySet();
				log.debug("Setting versions");
				setVersions(result, versionsMap, keys);
				result.setResult(SoftwareVersionsResult.Result.OK);
			} catch (Exception e) {
				log.error("Exception: " + e);
				result.setResult(SoftwareVersionsResult.Result.ERROR);
			}
		} else {
			log.info("Mock get software versions");
			result = Simulator.getSoftwareVersions();
		}
		return result;
	}

	@Override
	public synchronized DeviceTestResult deviceTest() {
		log.info("Call deviceTest");
		DeviceTestResult result;
		if (!(boolean) PosManagerConstantsAndEnums.DEVICE_TEST_MOCK) {
			result = new DeviceTestResult();
			try {
				log.info("deviceTest");
				ReconciliationData reconciliationResult = getPos().doReconciliation(
						PosManagerConstantsAndEnums.CB2_PRINT_ON_POS, PosManagerConstantsAndEnums.CB2_READ_RECEIPT);
				if (reconciliationResult.getResultCode().equals(TransactionDataResultCode.P00.getCode())) {
					result.setResult(DeviceTestResult.Result.OK);
				} else {
					result.setResult(DeviceTestResult.Result.ERROR);
				}
			} catch (Exception e) {
				log.error("Exception: " + e);
				result.setResult(DeviceTestResult.Result.ERROR);
			}
		} else {
			log.info("Mock device tests");
			result = Simulator.deviceTest();
		}
		return result;
	}

	// private methods
	private PosSisal getPos() throws Exception, IOException {
		log.info("Init tcp device factory");
		TcpDeviceFactory tcpDev = new TcpDeviceFactory();
		log.info("Getting tcp info");
		String tcpIp = (String) PosManagerConstantsAndEnums.DEVICE_IP;
		int tcpPort = (int) PosManagerConstantsAndEnums.DEVICE_PORT;
		log.debug("Set tcp data with ip: " + tcpIp + " and port: " + tcpPort);
		tcpDev.setTcpData(tcpIp, tcpPort);
		log.info("Getting device");
		PosSisal pos = new PosSisal(tcpDev.getDevice());
		pos.setDebug((int) PosManagerConstantsAndEnums.DEVICE_DEBUG);
		SisalPosLogger logger = new SisalPosLogger();
		pos.setLogger(logger);
		log.debug("Pos init ok");
		return pos;
	}

	private void versionsControl(TransactionData tdata) throws UpdateFirmwareException {
		if (!tdata.getPosVersions().substring(0, PosManagerConstantsAndEnums.VERSION_LENGHT)
				.equals(PosManagerConstantsAndEnums.ConfigurationBundle.FIRMWARE_VERSIONS)) {
			log.error("Exception: firmware need update - version expected: \""
					+ PosManagerConstantsAndEnums.ConfigurationBundle.FIRMWARE_VERSIONS + "\" - actual: \""
					+ tdata.getPosVersions().substring(0, PosManagerConstantsAndEnums.VERSION_LENGHT) + "\"");
			throw new UpdateFirmwareException("Update firmware pos needed");
		}
	}

	private void setPublicRsa(PosSisal pos) throws RsaException {
		try {
			log.info("Setting public rsa");
			TransactionData opResult = pos.doRSAKey(PosManagerConstantsAndEnums.RSA_CID,
					(int) PosManagerConstantsAndEnums.RSA_INDEX, (String) PosManagerConstantsAndEnums.PUBLIC_RSA,
					(String) PosManagerConstantsAndEnums.RSA_EXPONENT);

			if (!verifyResultCode(opResult)) {
				throw new RsaException("Error to set public rsa");
			}
		} catch (Exception e) {
			log.error("Exception to set rsa: " + e);
			throw new RsaException(e);
		}
	}

	private String[] cb2Configuration(PosSisal pos, TransactionData tdata, String tid) throws Cb2ConfException {
		String[] receipt = null;
		try {
			log.info("Cb2 control");
			log.info("Setting CB2");
			log.info("Cid: " + PosManagerConstantsAndEnums.CB2_CID);
			log.info("Pos Tid: " + tdata.getPosId());
			log.info("Original Tid: " + tid);
			tid = Utils.fillZeroSx(8, tid);
			log.info("Tid: " + tid);
			log.info("Company Code: " + PosManagerConstantsAndEnums.CB2_COMPANY_CODE);
			log.info("Url: " + PosManagerConstantsAndEnums.CB2_URL);
			log.info("Ip Address: " + PosManagerConstantsAndEnums.CB2_IP_ADDRESS);
			log.info("Tcp port: " + PosManagerConstantsAndEnums.CB2_TCP_PORT);
			log.info("Slot: " + PosManagerConstantsAndEnums.CB2_SLOT);
			log.info("Print on pos: " + PosManagerConstantsAndEnums.CB2_PRINT_ON_POS);
			log.info("Read Receipt: " + PosManagerConstantsAndEnums.CB2_READ_RECEIPT);
			log.info("Pos status: " + tdata.getStatus());
			if ((!tdata.getPosId().equals(tid)) || (!tdata.getStatus().equals(PosManagerConstantsAndEnums.CB2_STATUS_2)
					&& !tdata.getStatus().equals(PosManagerConstantsAndEnums.CB2_STATUS_7))) {
				TransactionData opResult = null;
				opResult = callCb2Command(pos, tid);
				log.debug("Cb2 result: " + opResult.getResultCode());
				if (!verifyResultCode(opResult)) {
					throw new Cb2ConfException("Error to set cb2");
				}
				log.debug("Cb2 receipt result: \"" + opResult.getLastReceipt() + "\"");
				receipt = getReceipt(opResult.getLastReceipt());
			}
		} catch (Exception e) {
			log.error("Exception to set cb2: " + e);
			throw new Cb2ConfException(e);
		}
		return receipt;
	}

	private TransactionData callCb2Command(PosSisal pos, String tid) {
		log.debug("Call cbVerify");
		pos.setPosId(PosManagerConstantsAndEnums.CB2_PASSE_PARTOUT);
		// TODO
		String a = "127.0.0.1"; //commonConfigService.getHostName(EndPoint.IPCB2),
		int b = 5001; //Integer.parseInt(commonConfigService.getHostName(EndPoint.PORTCB2)),
		
		return pos.doConfCb2(PosManagerConstantsAndEnums.CB2_CID, tid, 
				PosManagerConstantsAndEnums.CB2_COMPANY_CODE,
				PosManagerConstantsAndEnums.CB2_URL, 
				a,
				b,
				PosManagerConstantsAndEnums.CB2_SLOT, 
				PosManagerConstantsAndEnums.CB2_PRINT_ON_POS,
				PosManagerConstantsAndEnums.CB2_READ_RECEIPT);
	}

	private ElectronicPaymentResult paymentCommand(PosSisal pos, CardType cardType, int amount, long transactionId,
			String xid, PaymentType paymentType) {
		ElectronicPaymentResult electronicPaymentResult = new ElectronicPaymentResult();
		try {
			log.debug("original transactionId: \"" + transactionId + "\"");
			String transactionIdString = Utils.fillZeroSx(16, String.valueOf(transactionId));
			log.debug("new transactionId: \"" + transactionId + "\"");
			log.debug("xid: \"" + xid + "\"");
			log.debug("cardType.toString(): \"" + cardType.toString() + "\"");
			log.debug("amount: \"" + amount + "\"");
			log.debug("print on pos: \"" + PosManagerConstantsAndEnums.CB2_PRINT_ON_POS + "\"");
			log.debug("read receipt: \"" + PosManagerConstantsAndEnums.CB2_READ_RECEIPT + "\"");
			log.debug("paymentType: \"" + paymentType + "\"");
			TransactionData paymentResult = pos.doPayment(transactionIdString, xid, cardType.toString(), amount,
					PosManagerConstantsAndEnums.CB2_PRINT_ON_POS, PosManagerConstantsAndEnums.CB2_READ_RECEIPT,
					paymentType);
			if (paymentResult.getLastReceipt() != null) {
				electronicPaymentResult.setReceipt(getReceipt(paymentResult.getLastReceipt()));
				log.debug("Receipt");
				log.debug("-----------Inizio----------");
				for (String string : electronicPaymentResult.getReceipt()) {
					log.debug(string);
				}
				log.debug("------------Fine-----------");
			}
			if (verifyResultCode(paymentResult)) {
				int indexStart = paymentResult.getAddInfo().indexOf(PosManagerConstantsAndEnums.ADD_INFO_SEPARATOR) + 1;
				electronicPaymentResult.setAddInfo(paymentResult.getAddInfo().substring(indexStart, indexStart + 5));
				electronicPaymentResult.setAcquirer(paymentResult.getAcquirer());
				electronicPaymentResult.setCodAuth(paymentResult.getCodAuth());
				electronicPaymentResult.setDateTime(paymentResult.getGtDt());
				electronicPaymentResult.setResultCode(paymentResult.getResultCode());
				electronicPaymentResult.setResult(ElectronicPaymentResult.Result.OK);
				log.debug("AddInfo: " + electronicPaymentResult.getAddInfo());
				log.debug("Acquirer: " + electronicPaymentResult.getAcquirer());
				log.debug("CodAuth: " + electronicPaymentResult.getCodAuth());
				log.debug("DateTime: " + electronicPaymentResult.getDateTime());
				log.debug("ResultCode: " + electronicPaymentResult.getResultCode());
				log.debug("Result: " + electronicPaymentResult.getResult());
			} else {
				log.error("Error by pos for result code: " + paymentResult.getResultCode());
				electronicPaymentResult.setResult(ElectronicPaymentResult.Result.PAYMENT_ERROR);
			}
		} catch (Exception e) {
			log.error("Exception:" + e);
			electronicPaymentResult.setResult(ElectronicPaymentResult.Result.PAYMENT_ERROR);
		}

		return electronicPaymentResult;
	}

	private FiscalCodeResult verifyAndReadFiscalCode(int codeType) {
		FiscalCodeResult result;
		result = new FiscalCodeResult();
		try {
			log.debug("Code type: " + codeType);
			TransactionData data = getPos().doCFCNS(codeType);
			log.debug("Response: " + data);
			log.debug("Response result code: " + data.getResultCode());
			if (verifyResultCode(data)) {
				result = readRealFiscalCode(data);
			} else {
				log.error("Error by pos for result code: " + data.getResultCode());
				result.setResult(FiscalCodeResult.Result.READ_ERROR);
			}
		} catch (Exception e) {
			log.error("Exception: " + e);
			result.setResult(FiscalCodeResult.Result.READ_ERROR);
		}
		return result;
	}

	private FiscalCodeResult readRealFiscalCode(TransactionData data) {
		FiscalCodeResult result = new FiscalCodeResult();
		log.debug("card reading type: " + data.getCardReadingType());
		AcquisitionMode acquisitionMode = AcquisitionMode.getEnumByPaymentType(data.getCardReadingType());
		log.debug("Acquisition mode: " + acquisitionMode);
		result.setAcquisitionMode(acquisitionMode);
		if (acquisitionMode.equals(AcquisitionMode.ERROR)) {
			log.error("Error by pos for acquisition mode: " + acquisitionMode);
			result.setResult(FiscalCodeResult.Result.READ_ERROR);
		} else {
			FiscalCodeResult value = Utils.readCFCNSdata(data, acquisitionMode);
			result.setSurname(value.getSurname());
			result.setBirthDate(value.getBirthDate());
			result.setFiscalCode(value.getFiscalCode());
			result.setName(value.getName());
			result.setResult(FiscalCodeResult.Result.OK);
		}
		return result;
	}

	private void setVersions(SoftwareVersionsResult result, Map<PosVersions, String> versionsMap,
			Set<PosVersions> keys) {
		for (PosVersions key : keys) {
			switch (key) {
			case ECR:
				result.setCashManagerVersion(versionsMap.get(key));
				break;
			case EDL:
				result.setEcrDllVersion(versionsMap.get(key));
				break;
			case EMV:
				result.setPaymentApplicationVersion(versionsMap.get(key));
				break;
			case EXT:
				result.setExternalManagerVersion(versionsMap.get(key));
				break;
			case MAN:
				result.setManagerVersion(versionsMap.get(key));
				break;
			case MST:
				result.setMasterVersion(versionsMap.get(key));
				break;
			case SSA:
				result.setSystemSecurityApplicationVersion(versionsMap.get(key));
				break;
			case SSL:
				result.setSslManagerVersion(versionsMap.get(key));
				break;
			case SYS:
				result.setOperatingSystemVersion(versionsMap.get(key));
				break;
			}
		}
	}

	private ElectronicPaymentResult electronicPayment(PosSisal pos, CardType cardType, String xid, int amount,
			long transactionId, ElectronicPaymentResult electronicPaymentResult, PaymentType paymentType) {
		log.debug("Call Verify and Pay");
		TransactionData tdata = pos.doStatus();
		log.debug("Called do status");
		if (tdata != null) {
			log.debug("Pos status: " + tdata.getStatus());
			if (tdata.getStatus().equals(PosManagerConstantsAndEnums.CB2_STATUS_2)
					|| tdata.getStatus().equals(PosManagerConstantsAndEnums.CB2_STATUS_7)) {
				boolean reconciliationok = verifyAndDoReconciliation(pos, tdata);
				if (reconciliationok) {
					log.debug("Amount: " + amount);
					if ((boolean) PosManagerConstantsAndEnums.PAYMENT_TEST) {
						amount = 1000;
						log.debug("Amount modified for test: " + amount);
					}
					int amountForPos = amount / 10;
					log.debug("Amount: " + amount);
					log.debug("Amount for pos: " + amountForPos);
					electronicPaymentResult = paymentCommand(pos, cardType, amountForPos, transactionId, xid,
							paymentType);
				} else {
					log.error("Reconciliation failed");
					electronicPaymentResult.setResult(ElectronicPaymentResult.Result.PAYMENT_ERROR);
				}
			}
		} else {
			log.error("Pos null");
			electronicPaymentResult.setResult(ElectronicPaymentResult.Result.PAYMENT_ERROR);
		}
		return electronicPaymentResult;
	}

	private PaymentType selectPaymentType(String ptype) {
		PaymentType paymentType;
		String paymentTypeMock = (String) PosManagerConstantsAndEnums.POS_PAYMENT_TYPE;
		if (paymentTypeMock.equals("auto")) {
			log.debug("Payment Type: " + ptype);
			switch (ptype) {
			case "PB":
				paymentType = PaymentType.ONLY_DEBIT_1;
				break;
			case "CC":
				paymentType = PaymentType.ONLY_CREDIT_2;
				break;
			case "DP":
				paymentType = PaymentType.BOTH_CLESS_4;
				break;
			default:
				paymentType = PaymentType.BOTH_0;
				break;
			}
			log.debug("Payment Type: " + paymentType);
		} else {
			log.info("Payment Mock: " + paymentTypeMock);
			paymentType = PaymentType.valueOf(paymentTypeMock);
		}
		return paymentType;
	}

	private boolean verifyAndDoReconciliation(PosSisal pos, TransactionData tdata) {
		boolean response = true;
		log.debug("Reconciliation pos status: " + tdata.getStatus());
		if (tdata.getStatus().equals(PosManagerConstantsAndEnums.CB2_STATUS_7)) {
			log.debug("Call reconciliation");
			ReconciliationData reconciliationResult = pos.doReconciliation(PosManagerConstantsAndEnums.CB2_PRINT_ON_POS,
					PosManagerConstantsAndEnums.CB2_READ_RECEIPT);
			log.debug("Reconciliation status: " + reconciliationResult.getResultCode());
			if (!reconciliationResult.getResultCode().equals(TransactionDataResultCode.P00.getCode())) {
				response = false;
			}
		}
		return response;
	}

	private String[] getReceipt(String originalReceiptStr) {
		StringBuilder originalReceipt = new StringBuilder(originalReceiptStr);
		int checkPoint = (Integer) PosManagerConstantsAndEnums.POS_RECEIPT_SPLIT;
		int sizeStringArray = (originalReceipt.length() / checkPoint);
		log.info("Original Receipt length: " + originalReceipt.length());
		log.debug("Original receipt by POS:" + originalReceipt);
		log.debug("Char of original POS Receipt");
		LinkedList<Integer> indexToRemoveBadCharacter = new LinkedList<>();
		char[] receiptChar = originalReceipt.toString().toCharArray();
		for (int i = 0; i < receiptChar.length; i++) {
			char c = receiptChar[i];
			log.debug(c + " char to int ->" + (int) c);
			if (c == 127) {
				log.debug("Character to exlude: {}", i);
				indexToRemoveBadCharacter.add(i);
			}
		}
		for (int i = indexToRemoveBadCharacter.size() - 1; i >= 0; i--) {
			int index = indexToRemoveBadCharacter.get(i);
			log.debug("Substring from 0 to {} and from {} to {} ", index, (index + 1), originalReceipt.length());
			originalReceipt = new StringBuilder(originalReceipt.substring(0, index)
					+ originalReceipt.substring(index + 1, originalReceipt.length()));
		}
		log.info("CheckPoint: " + checkPoint);
		log.info("Size array: " + sizeStringArray);
		String[] receipt = new String[sizeStringArray];
		int start = 0;
		int end = start + checkPoint;
		log.debug("Original receipt: " + originalReceipt);
		for (int i = 0; i < sizeStringArray; i++) {
			receipt[i] = originalReceipt.substring(start, end);
			start += checkPoint;
			end += checkPoint;
			log.debug("Receipt - riga " + i + " :" + receipt[i]);
		}
		return receipt;
	}

	private boolean verifyResultCode(TransactionData opResult) {
		return opResult.getResultCode().equals(TransactionDataResultCode.P00.getCode());
	}

	/*public void setCommonConfigService(CommonConfigService commonConfigService) {
		this.commonConfigService = commonConfigService;
	}

	public void unsetCommonConfigService(CommonConfigService commonConfigService) {
		this.commonConfigService = null;
	}*/

	public void setConfigurationManagerService(ConfigurationManagerService configurationManagerService)
			throws IOException {
		configurationManagerService.registerConfigurable(this);
	}

	public void unsetConfigurationManagerService(ConfigurationManagerService configurationManagerService) {

	}

	@Override
	public boolean isPosHwConnected() {
		// File posFile = new File(POS_PATH).getAbsoluteFile();
		// TODO SISAL. as discussed with Luigi Spinelli we will skip this check waiting
		// N&ETS library to provide a more stable check to define if POS is connected.
		Boolean bResult = true;// (posFile != null && posFile.exists());
		return bResult;
	}
}
