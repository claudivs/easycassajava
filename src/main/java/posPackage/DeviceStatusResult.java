package posPackage;

public class DeviceStatusResult {
	public enum Result {
		NOT_CONNECTED("POS device is not connected"),
		OK("POS device is working properly"),
		PAYMENTS_DISABLED("Payments disabled, input functionalities working properly"),
		CRITICAL_ERROR("Not recoverable error, device shall be sent back to OEM");

		private final String description;

		Result(final String description) {
			this.description = description;
		}

		@Override
		public String toString() {
			return this.description;
		}    
	}

	private Result result;

	public void setResult(Result success) {
		this.result = success;
	}

	public Result getResult() {
		return this.result;
	}
}
