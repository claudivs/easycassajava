package posPackage;

public class CommonConfigServiceConstants {

	private CommonConfigServiceConstants() {
	}

	public static final String EXECUTECOMMAND_DEVICEINFO = "dmidecode --string processor-version";
	public static final String WAVEDEVICE = "amd";
	public static final String BIGTOUCH = "atom";
	public static final String FUSION = "celeron";
	public static final String SERAILNUMBERCOMMNAD = "dmidecode --string system-serial-number";
	public static final String DEFAULT_SERIAL_NUM = "default_device_serial";

	public static final String BE_ENVIRONMENT_KEY = "sisal.environment";
	public static final String DEFAULT_BE_ENVIRONMENT = "DEV";

	public static final int WAVE_SERIAL_BEGIN_INDEX = 3;

	public static final String CONFIG_OVERRIDE_FILE = "override_properties_file";

	public static final String MACADDRESSFILE = "/sys/class/net/%s/address";
	public static final String DEFAULTETHADAPTER = "eth0";
	public static final String SECONDARYETHADAPTER = "eth1";
}
