package posPackage;

public interface PosManager {

	/**
	 * The method performs POS initialization
	 * @return {@link InitResult}
	 */
	public InitResult init();
	
	/**
	 * Retrieve POS device status
	 * 
	 * @return {@link DeviceStatusResult}
	 */
	public DeviceStatusResult deviceStatus();
	
	/**
	 * This function will be called when the system needs to perform an electronic payment with POS device.
	 * Request an electronic payment for the input card type, amount and transaction type (BILLING or SPENDING), 
	 * and get electronic payment receipt data and bank data from device.
	 * @param cardType {@link CardType}
	 * @param xid identificativo servizio
	 * @param amount
	 * @param transactionId
	 * @param transactionType {@link TransactionType}
	 * @param pType {@link PaymentType}
	 * @return {@link ElectronicPaymentResult}
	 */
	public ElectronicPaymentResult electronicPayment(CardType cardType, String xid, int amount, long transactionId, TransactionType transactionType, String ptype);
	
	/**
	 * This function will be called when the system needs to read a card Primary Account Number (PAN). 
	 * Only POS device will have visibility on the unencrypted data, and will return a masked and an encrypted version
	 * @param panMask input of mask to be used to hide PAN digits. 
	 * If input mask hides too few PAN digits, this function shall return an error
	 * @return {@link SecuredCardDataResult}
	 */
	public SecuredCardDataResult readSecuredCardData(String panMask);
	
	/**
	 * This function will be called when the system needs to read operator smart cart to authorize maintenance access.
	 * @return {@link OperatorSmartCardResult}
	 */
	public OperatorSmartCardResult readOperatorSmartCard();
	
	/**
	 * This function will be called when the system needs to read health insurance card to get customer fiscal code and personal data.
	 * @param codeType
	 * @return {@link FiscalCodeResult}
	 */
	public FiscalCodeResult readFiscalCode(int codeType);
	
	/**
	 * This function will be called to get data required to reprint the last printed electronic payment receipt.
	 * @return {@link LastElectronicPaymentReceiptResult}
	 */
	public LastElectronicPaymentReceiptResult getLastElectronicPaymentReceipt();
	
	/**
	 * This function will be called when the system needs to get POS software versions information.
	 * @return {@link SoftwareVersionsResult}
	 */
	public SoftwareVersionsResult getSoftwareVersions();
	
	/**
	 * This function will be called when the system needs to test POS device.
	 * @return
	 */
	public DeviceTestResult deviceTest();

	/**
	 * This method check if the POS HARDWARE (ONLY) is connected
	 */
	public boolean isPosHwConnected();

}
