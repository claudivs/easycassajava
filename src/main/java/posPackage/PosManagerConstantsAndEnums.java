package posPackage;

import java.util.HashMap;

public class PosManagerConstantsAndEnums {
	//OTHER

	
	public static final String DEVICE_IP = "127.0.0.1";
	public static final int DEVICE_PORT = 5001;
	public static final int DEVICE_DEBUG = 10;
	public static final String FIRMWARE_VERSIONS = "SYS32.10MAN78.06EMV152AMECR08.83SSL01.47EXT05.23EDL03.89P";
	public static final int RSA_INDEX = 1;
	public static final String PUBLIC_RSA = "A7076ABDAF672F32BC0D29FF25D8E029504946E5F4D127AECC0E05234E3C72BB2C4AD5282E2FF96D687F9FB933D42CA835A710BE502A436C18B36EAEB657DCDD6F33F637D7C597BC3BFF131CF3BC001CBCE8B83F55B674E533C7A82E570226EA7EE45A6AAACD05EA05CED36A6CB3649F7E3BBC42D22AD9E312D21821EC57AFFFA6C4EB1AEF7A40F3C36EAFB8522534E7330DA3CFE4DC2D33E26D92E39461429008FF963472B07FDE93AA6343D6DE81CFB2B9049D51AEF0E6ABB5AB74FA7A42BEB48C67E13F8527243DA2307F6B12F7523537A9A00C67E68363F64A68E754CEC4714ABA7873EFF897102949404407D76BF5A690D248A7750B511F450CD9B6AB95";
	//public static final String RSA_EXPONENT = "3";
	public static final String CB2_IP_ADDRESS = "010.007.003.041";
	public static final int CB2_TCP_PORT = 10501;
	public static final int INPUT_LENGHT = 16;
	public static final int CONF_TYPE = 0;
	public static final int TIME_OUT = 90;
	public static final int SMART_CARD_WAIT = 3;
	public static final int SMART_CARD_PSC_TYPE = 0;
	public static final String SMART_CARD_PSC = "FA3456";
	public static final String SMART_CARD_MESSAGE = "Insert your card";
	public static final boolean INIT_MOCK = false;
	public static final boolean DEVICE_STATUS_MOCK = false;
	public static final boolean ELECTRONIC_PAYMENT_MOCK = false;
	public static final boolean READ_SECURED_CARD_DATA_MOCK = false;
	public static final boolean READ_OPERATOR_SMART_CARD_MOCK = false;
	public static final boolean READ_FISCAL_CODE_MOCK = false;
	public static final boolean GET_LAST_ELECTRONIC_PAYMENT_RECEIPT_MOCK = false;
	public static final boolean GET_SOFTWARE_VERSIONS_MOCK = false;
	public static final boolean DEVICE_TEST_MOCK = false;
	public static final boolean PAYMENT_TEST = true;    
	public static final String POS_PAYMENT_TYPE= "auto";
	public static final int POS_RECEIPT_SPLIT = 24;
	public static final boolean PAYMENT_ECR = true;
	

	//RSA Constants
	public static final String RSA_CID = "12345678";
	public static final String RSA_EXPONENT = "000003";
	
	//CB2 Constants
	public static final String CB2_STATUS_2 = "2";
	public static final String CB2_STATUS_7 = "7";
	public static final String CB2_CID = "00000001";
	public static final String CB2_PASSE_PARTOUT = "00000000";
	public static final String CB2_COMPANY_CODE = "88105";
	public static final String CB2_URL = "";
	public static final String CB2_SLOT = "";
	public static final boolean CB2_PRINT_ON_POS = false;
	public static final boolean CB2_READ_RECEIPT = true;
	
	//Data lenght
	public static final int INITIAL_DATA_CHARACTER_TO_DELETE = 32;
	public static final int CHARACTER_TO_READ_LENGHT_NEXT_FIELD = 2;
	public static final int CHARACTER_TO_JUMP_FOR_CF = 7;
	public static final int CHARACTER_TO_CF = 16;
	public static final int CHARACTER_TO_START_BIRTHDATE = 6;
	public static final int CHARACTER_TO_END_BIRTHDATE = 11;
	public static final int CHARACTER_TO_START_YEAR = 0;
	public static final int CHARACTER_TO_END_YEAR = 2;
	public static final int CHARACTER_TO_START_MONTH = 2;
	public static final int CHARACTER_TO_END_MONTH = 3;
	public static final int CHARACTER_TO_START_DAY = 3;
	public static final int YEAR_TO_SUM = 2000;
	public static final int WOMAN_DAY = 40;
	public static final int YEAR_OLD_FOR_OPERATION = 100;
	public static final String YEAR_ADDING_FOR_OLD_MAN = "19";
	public static final String YEAR_ADDING_FOR_YOUNG_MAN = "20";
	public static final int VERSION_LENGHT = 57;
	
	//ADD INFO Constants
	public static final String ADD_INFO_SEPARATOR = "[";
	
	
	
	
	public enum ConfigurationBundle{
		DEVICE_IP					("device.ip"),
		DEVICE_PORT					("device.port"), 
		DEVICE_DEBUG				("device.debug"), 
		FIRMWARE_VERSIONS			("firmware.versions"),
		PUBLIC_RSA					("public.rsa"),
		//IPADDRESS e PORT: cambiano a seconda dell’ambiente in cui si lavora (in DEV “010.000.011.050” e “10501”, in TEST “010.007.003.041” e “10501”, in PROD “010.007.000.077” e “22501”)
		CB2_IP_ADDRESS				("cb2.ip.address"),
		CB2_TCP_PORT				("cb2.tcp.port"), 
		SMART_CARD_WAIT				("smart.card.wait"),
		SMART_CARD_PSC_TYPE			("smart.card.psc.type"),
		SMART_CARD_PSC				("smart.card.psc"),
		SMART_CARD_MESSAGE			("smart.card.message"),
		FISCAL_CODE_TYPE			("fiscal.code.type"),
		PAYMENT_ECR					("payment.ecr"),
		RSA_INDEX					("rsa.index"),
		RSA_EXPONENT				("rsa.exponent"),
		INPUT_LENGHT				("input.lenght"),
		CONF_TYPE					("conf.type"),
		TIME_OUT					("timeout"), 
		POS_PAYMENT_TYPE			("pos.payment.type"),
		POS_RECEIPT_SPLIT			("pos.receipt.split"),
		;
		
		private String code;
		
		private ConfigurationBundle(String code) {
			this.code = code;
		}

		public String getCode() {
			return code;
		}
	}
	
	public enum MockProperties{
		INIT_MOCK									("init.mock"),
		DEVICE_STATUS_MOCK							("device.status.mock"),
		ELECTRONIC_PAYMENT_MOCK						("electronic.payment.mock"),
		READ_SECURED_CARD_DATA_MOCK					("read.secured.card.data.mock"),
		READ_OPERATOR_SMART_CARD_MOCK				("read.operator.smart.card.mock"),
		READ_FISCAL_CODE_MOCK						("read.fiscal.code.mock"),
		GET_LAST_ELECTRONIC_PAYMENT_RECEIPT_MOCK	("get.last.electronic.payment.receipt.mock"),
		GET_SOFTWARE_VERSIONS_MOCK					("get.software.versions.mock"),
		DEVICE_TEST_MOCK							("device.test.mock"),
		PAYMENT_TEST								("payment.test")
		;
		
		private String code;
		
		private MockProperties(String code) {
			this.code = code;
		}

		public String getCode() {
			return code;
		}
	}
	
	public enum TransactionDataStatus {
		STATUS_NULL ("null"),
		STATUS_0 ("0"),
		STATUS_1 ("1"),
		STATUS_2 ("2"),
		STATUS_3 ("3"),
		STATUS_4 ("4"),
		STATUS_5 ("5"),
		STATUS_6 ("6"),
		STATUS_7 ("7"),
		STATUS_8 ("8"),
		STATUS_9 ("9"),
		;
		
		private String code;
		
		private TransactionDataStatus(String code) {
			this.code = code;
		}

		public String getCode() {
			return code;
		}
		
		private static HashMap<String, TransactionDataStatus> codeValueMap = new HashMap<String, TransactionDataStatus>(11);
		static
	    {
	        for (TransactionDataStatus  type : TransactionDataStatus.values())
	        {
	            codeValueMap.put(type.getCode(), type);
	        }
	    }
		public static TransactionDataStatus getEnumByCode(String code) {
			return codeValueMap.get(code);
		}
	}
	
	public enum PosVersions{
		SSA,
		EMV,
		SSL,
		MAN,
		ECR,
		MST,
		SYS,
		EXT,
		EDL;
		
	}
	
	public enum TransactionDataResultCode{
		P00 ("P00"), // Successfully response from POS
		P01 ("P01"), // Unsuccessfully response from POS
		P09 ("P09"), // Unsuccessfully response from POS
		A01 ("A01"), // Error opening device
		A02 ("A02"), // Device already open
		A03 ("A03"), // Error sending request to POS
		A04 ("A04"), // Timeout receiving response from POS
		A05 ("A05"), // Response discarded
		A06 ("A06"), // Pos is processing another command
		A10 ("A10"), // Timeout receiving command ack
		A11 ("A11"), // Timeout receiving additional info ack
		A12 ("A12"), // Timeout receiving result
		A13 ("A13"), // Timeout waiting ack on open connection
		A14 ("A14"), // Timeout waiting close connection
		A16 ("A16"), // Timeout receiving result (ssl)
		A17 ("A17"), // Timeout receiving additional info
		;
		
		private String code;
		
		private TransactionDataResultCode(String code) {
			this.code = code;
		}

		public String getCode() {
			return code;
		}
	}
	
	public enum CfMonth{
		A	("01"),
		B	("02"),
		C	("03"),
		D	("04"),
		E	("05"),
		H	("06"),
		L	("07"),
		M	("08"),
		P	("09"),
		R	("10"),
		S	("11"),
		T	("12");
		
		private String month;
		private CfMonth(String month) {
			this.month = month;
		}
		
		public String getMonth() {
			return month;
		}
	}
}
