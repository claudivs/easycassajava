package posPackage;

public enum TransactionType {
	BILLING("Billing transaction", "BIL"),
	SPENDING("Spending transaction", "SPN");

	private final String description;
	private final String xidCode;

	TransactionType(final String description, String xidCode) {
		this.description = description;
		this.xidCode = xidCode;
	}

	public String getXidCode() {
		return xidCode;
	}
	
	@Override
	public String toString() {
		return this.description;
	}    
}

