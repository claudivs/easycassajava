package posPackage;

public enum PlatformEnvironment {
	DEV("DEV"),
	SIT("SIT"),
	PRE_PROD("PRE_PROD"),
	PROD("PROD");
	
	private String value;
	private PlatformEnvironment(String value) {
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
	
	static PlatformEnvironment fromStringToEnv(String input) {
		PlatformEnvironment result = DEV;
		
		for(PlatformEnvironment env : values()) {
			if(env.getValue().equals(input)) {
				result = env;
				break;
			}
		}
		
		return result;
	}
}
