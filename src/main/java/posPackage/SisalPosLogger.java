package posPackage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.netsw.coms.device.LogWrapper;

public class SisalPosLogger implements LogWrapper{

	private Logger log = LoggerFactory.getLogger(getClass());
	
	@Override
	public void debug(String arg0) {
		log.debug(arg0);
	}

	@Override
	public void error(String arg0) {
		log.error(arg0);
	}

	@Override
	public void error(String arg0, Throwable arg1) {
		log.error(arg0, arg1);
	}

	@Override
	public void info(String arg0) {
		log.info(arg0);
	}

	@Override
	public void warn(String arg0) {
		log.warn(arg0);
	}

}
