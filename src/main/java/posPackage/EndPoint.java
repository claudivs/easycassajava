package posPackage;

public enum EndPoint {
	PING("sisal.environment.PING"),
	IS("sisal.environment.IS"),
	AM("sisal.environment.AM"),
	ST_RES("sisal.environment.ST_RES"),
	BACHECA("sisal.environment.BACHECA"),
	PGT("sisal.environment.PGT"),
	LOTTERY("sisal.environment.LOTTERY"),
	PERSEO("sisal.environment.PERSEO"),
	IPCB2("sisal.environment.IPCB2"),
	PORTCB2("sisal.environment.PORTCB2");
	
	private String value;
	
	private EndPoint(String value) {
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
}
