
package posPackage;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PackageList {

    @SerializedName("Tag")
    @Expose
    private String tag;
    @SerializedName("FileName")
    @Expose
    private String fileName;
    @SerializedName("Version")
    @Expose
    private String version;

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

}
