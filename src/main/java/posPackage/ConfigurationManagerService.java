package posPackage;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

public class ConfigurationManagerService {
	private static final String TEMP_FOLDER = "/var/configuration/";

	private static final String JSON_EXTENSION = ".json";

	private static Logger logger = LoggerFactory.getLogger(ConfigurationManagerService.class);

	private Map<String, SisalConfigurableComponent> configurableMap = new HashMap<>();

//	protected void activate(Map<String, Object> properties) {
//		logger.debug("activate >> <<");
//	}
//
//	protected void deactivate() {
//		logger.debug("deactivate >> <<");
//	}

	/**
	 * This method unregister a bundle from configuration management
	 * 
	 * @param configurableClass
	 *            the service class for the bundle to unregister
	 * @return
	 */
	public void unregisterConfigurable(SisalConfigurableComponent configurableClass) {
		configurableMap.remove(configurableClass.getClass().getName());
		logger.debug("unregisterConfigurable >> Removed configurable {}", configurableClass.getClass().getName());
	}

	/**
	 * This method register a bundle, setting its properties from the JSON file
	 * included in the jar, and look in the TEMP_FOLDER if there is a new set of
	 * properties that need to be uploaded to the original set
	 * 
	 * @param configurableClass
	 *            the service class for the bundle to register
	 * @throws IOException
	 */
	public void registerConfigurable(SisalConfigurableComponent configurableClass) throws IOException {
		logger.info("registerConfigurable >> {}", configurableClass.getClass());

		configurableClass.updateConfigMap(getAllConfigs(configurableClass));
		configurableMap.put(configurableClass.getClass().getName(), configurableClass);
		logger.debug("registerConfigurable >> {} added to map", configurableClass.getClass().getName());

	}

	private File getTempFile(String pid) {
		String path = TEMP_FOLDER + pid + JSON_EXTENSION;
		logger.debug("getTempFile >> Looking for file: {}", path);
		return new File(path);
	}

	/**
	 * This method update the selected config in existing properties
	 * 
	 * @param pid
	 *            the name of the service class for the bundle to register
	 * @param configList
	 *            new set of configuration, as a java.util.List<Config>
	 * @throws IOException
	 */
	public boolean updateSpecifcProperties(String pid, List<Config> configList) throws IOException {
		logger.debug("updateSpecifcProperties >>");
		boolean result = false;
		if (configurableMap.containsKey(pid) && updateAndStoreConfig(configurableMap.get(pid), configList)) {

			configurableMap.get(pid).update(configurableMap.get(pid).getProperties());
			result = true;

		} else {
			logger.error("updateSpecifcProperties >> pid {} to be updated with config is not found in map", pid);
		}
		logger.debug("updateSpecifcProperties <<");

		return result;
	}

	private boolean updateAndStoreConfig(SisalConfigurableComponent sisalConfigurableComponent,
			List<Config> configToUpdateList) throws IOException {
		boolean result = false;
		if (sisalConfigurableComponent != null && configToUpdateList != null) {
			// update the stored in files system config
			File tempFile = getTempFile(sisalConfigurableComponent.getClass().getName());
			List<Config> storedConfigList = getConfigListFromJson(tempFile);

			Map<String, Config> storedConfigMap = new HashMap<>();

			if (storedConfigList != null && !storedConfigList.isEmpty()) {
				for (Config config : storedConfigList) {
					if (config != null && config.getName() != null) {
						storedConfigMap.put(config.getName(), config);
					}
				}
			}

			if (configToUpdateList != null && !configToUpdateList.isEmpty()) {
				for (Config config : configToUpdateList) {
					if (config != null && config.getName() != null) {
						storedConfigMap.put(config.getName(), config);
					}
				}
			}

			List<Config> allStoredConfigsList = new ArrayList<>(storedConfigMap.values());
			ConfigContainer configContainer = new ConfigContainer();
			configContainer.setConfig(allStoredConfigsList);

			/*FileUtils.writeStringToFile(tempFile,
					GsonEngineFactory.getGsonGregorianDisabledHtml().toJson(configContainer), false);*/

			// now update all the configs in RAM
			sisalConfigurableComponent.updateConfigMap(storedConfigMap);
			result = true;
		} else {
			logger.error("updateStoredConfig >> invalid data");
		}

		return result;
	}

	private List<Config> getConfigListFromInputStream(InputStream stream) throws IOException {
		List<Config> configList = null;

		if (stream != null) {
			String jsonProperties = IOUtils.toString(stream);
			ConfigContainer configContainer = new Gson().fromJson(jsonProperties, ConfigContainer.class);

			if (configContainer != null)
				configList = configContainer.getConfig();

		} else {
			logger.warn("getConfigListFromInputStream >> stream is null");
		}


		return configList;
	}

	protected List<Config> getConfigListClassResources(SisalConfigurableComponent configurableClass) throws IOException {

		List<Config> configs;
		try(InputStream stream = configurableClass.getClass().getClassLoader()
				.getResourceAsStream(configurableClass.getClass().getName().concat(JSON_EXTENSION))){

			configs = getConfigListFromInputStream(stream);
		}

		return configs;
	}

	protected List<Config> getConfigListFromJson(File file) throws IOException {
		List<Config> result = null;

		if (file != null && file.exists() && !file.isDirectory()) {
			// read configurations from file
			logger.debug("getConfigListFromJson >> Read configurations list from filesystem {}", file.getName());
			try(InputStream stream = new FileInputStream(file.getAbsolutePath())){

				if (stream != null) {
					result = getConfigListFromInputStream(stream);
				}
			}
		}

		return result;
	}

	/**
	 * Returns the map of specific pid
	 * 
	 * @param pid
	 *            - component name
	 * @return properies - Map contains properties of specific pid
	 */
	public Map<String, Object> getPIDProperties(String pid) {
		Map<String, Object> properties = null;
		if (configurableMap != null && !configurableMap.isEmpty() && configurableMap.containsKey(pid)
				&& configurableMap.get(pid) != null) {
			properties = configurableMap.get(pid).getProperties();

		}
		return properties;
	}

	public List<Config> getPIDConfigList(String pid) {

		List<Config> configList = null;
		if (configurableMap != null && !configurableMap.isEmpty() && configurableMap.containsKey(pid)
				&& configurableMap.get(pid) != null) {
			configList = new ArrayList<>(configurableMap.get(pid).getConfigMap().values());
		}
		return configList;
	}

	private Map<String, Config> getAllConfigs(SisalConfigurableComponent sisalConfigurableComponent)
			throws IOException {

		Map<String, Config> allConfigs = new LinkedHashMap<>();
		List<Config> resourcesConfig = getConfigListClassResources(sisalConfigurableComponent);

		if (resourcesConfig != null && !resourcesConfig.isEmpty()) {
			for (Config config : resourcesConfig) {
				if (config != null && config.getName() != null) {

					allConfigs.put(config.getName(), config);
				}
			}
		}
		File tempFile = getTempFile(sisalConfigurableComponent.getClass().getName());
		List<Config> storedConfig = getConfigListFromJson(tempFile);

		if (storedConfig != null && !storedConfig.isEmpty()) {
			for (Config config : storedConfig) {
				if (config != null && config.getName() != null) {
					allConfigs.put(config.getName(), config);
				}
			}
		}

		return allConfigs;
	}

	/**
	 * gets the list of configured pids
	 * 
	 * @return configuredPids - list of component names
	 */
	public List<String> getConfiguredPIDs() {
		List<String> configuredPids = new ArrayList<>();

		if (configurableMap != null && !configurableMap.isEmpty()) {
			Iterator<Entry<String, SisalConfigurableComponent>> it = configurableMap.entrySet().iterator();
			while (it.hasNext()) {
				Entry<String, SisalConfigurableComponent> entry = it.next();
				if (entry.getKey() != null && !entry.getKey().isEmpty()) {
					configuredPids.add(entry.getKey());
				}
			}
		}
		return configuredPids;
	}
}
