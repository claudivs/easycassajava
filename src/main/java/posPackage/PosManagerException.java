package posPackage;

public class PosManagerException extends Exception {

	private static final long serialVersionUID = -8435642941836590648L;

	public PosManagerException() {
	}

	public PosManagerException(Exception e) {
		super(e);
	}
	
	public PosManagerException(String e) {
		super(e);
	}

}
