
package posPackage;

import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Config {

	private String name;
	private String value;
	private String type;
	private JsonObject structuredValue;

	/**
	 * No args constructor for use in serialization
	 * 
	 */
	public Config() {
	}

	/**
	 * 
	 * @param name
	 * @param value
	 * @param type
	 */
	public Config(String name, String value, String type, JsonObject structuredValue) {
		super();
		this.name = name;
		this.value = value;
		this.type = type;
		this.structuredValue = structuredValue;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public JsonObject getStructuredValue() {
		return structuredValue;
	}

	public void setStructuredValue(JsonObject structuredValue) {
		this.structuredValue = structuredValue;
	}
	
	public enum TypeEnum {
		INTEGER("Integer"), BOOLEAN("Boolean"), STRING("String"), STRUCT("Struct"), UNKNOW("");

		private TypeEnum(String value) {
			this.value = value;
		}

		private final String value;

		public String getValue() {
			return value;
		}

		public static TypeEnum getByValue(String inputType) {
			TypeEnum[] types = TypeEnum.values();
			for (TypeEnum type : types) {
				if (type.getValue().equalsIgnoreCase(inputType)) {
					return type;
				}
			}
			return UNKNOW;
		}
	}
}
