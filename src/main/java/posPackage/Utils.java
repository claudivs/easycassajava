package posPackage;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import posPackage.Config.TypeEnum;
import posPackage.FiscalCodeResult;
import posPackage.FiscalCodeResult.AcquisitionMode;
import posPackage.PosManagerConstantsAndEnums.CfMonth;
import posPackage.PosManagerConstantsAndEnums.PosVersions;

import it.netsw.apps.ftfs.posdealer.impl.TransactionData;

public class Utils {

	private static Logger log = LoggerFactory.getLogger(Utils.class);

	public static Map<PosVersions, String> getPosVersionsMap(String versions) {
		Map<PosVersions, String> result = new HashMap<PosVersions, String>();
		Map<Integer, String> positionKeyMap = new HashMap<>();
		for (PosVersions version : PosVersions.values()) {
			int index = versions.indexOf(version.toString());
			if (index != -1) {
				positionKeyMap.put(index, version.toString());
			}
		}
		SortedSet<Integer> keys = new TreeSet<>(positionKeyMap.keySet());
		Iterator<Integer> it = keys.iterator();
		Integer precKey = null;
		while (it.hasNext()) {
			Integer key = it.next();
			if (precKey != null) {
				result.put(PosVersions.valueOf(positionKeyMap.get(precKey)), versions.substring(precKey + 3, key));
			}
			precKey = key;
		}
		result.put(PosVersions.valueOf(positionKeyMap.get(keys.last())),
				versions.substring(keys.last() + 3, versions.length()));
		return result;
	}

	public static FiscalCodeResult readCFCNSdata(TransactionData data, AcquisitionMode acquisitionMode) {
		FiscalCodeResult result = new FiscalCodeResult();
		log.debug("Acquisition mode: " + acquisitionMode);
		log.debug("Card Data: \"" + data.getCardData() + "\"");
		switch (acquisitionMode) {
		case CHIP:
			DinamicData dinamicData = getDinamicValueOfData(
					data.getCardData().trim().substring(PosManagerConstantsAndEnums.INITIAL_DATA_CHARACTER_TO_DELETE));
			result.setSurname(dinamicData.getValue());
			dinamicData = getDinamicValueOfData(dinamicData.getData());
			result.setName(dinamicData.getValue());
			dinamicData = getDinamicValueOfData(dinamicData.getData());
			result.setBirthDate(formatBirthDate(dinamicData.getValue()));
			result.setFiscalCode(dinamicData.getData().substring(PosManagerConstantsAndEnums.CHARACTER_TO_JUMP_FOR_CF,
					PosManagerConstantsAndEnums.CHARACTER_TO_JUMP_FOR_CF
							+ PosManagerConstantsAndEnums.CHARACTER_TO_CF));
			log.debug("Cognome: " + result.getSurname());
			log.debug("Nome: " + result.getName());
			log.debug("Data nascita: " + result.getBirthDate());
			log.debug("CF: " + result.getFiscalCode());
			break;

		case MAGNETIC_STRIPE:
			result.setFiscalCode(data.getCardData().trim().substring(0, PosManagerConstantsAndEnums.CHARACTER_TO_CF));
			char[] cardData = data.getCardData().trim().substring(PosManagerConstantsAndEnums.CHARACTER_TO_CF)
					.toCharArray();
			StringBuilder surname = new StringBuilder();
			StringBuilder name = new StringBuilder();
			boolean letteraCognome = true;
			for (int i = 0; i < cardData.length; i++) {
				char c = cardData[i];
				log.debug("Char: \"" + c + "\"");
				if (c == ' ' && cardData.length >= i + 1 && cardData[i + 1] == ' ') {
					log.debug("Separatore cognome trovato: doppio spazio");
					letteraCognome = false;
					i++;
				} else {
					if (letteraCognome) {
						surname.append(c);
					} else {
						name.append(c);
					}
				}
			}
			result.setName(name.toString());
			result.setSurname(surname.toString());
			result.setBirthDate(getBirthDateByCf(result.getFiscalCode()));
			log.debug("CF: " + result.getFiscalCode());
			log.debug("Cognome: " + result.getSurname());
			log.debug("Nome: " + result.getName());
			log.debug("BirthDate: " + result.getBirthDate());
			break;

		case MANUAL:
			log.info("TODO: stop implement for dataless");
			break;

		default:
			log.error("Acquisition mode not exist: " + acquisitionMode);
			result.setResult(FiscalCodeResult.Result.READ_ERROR);
			break;
		}

		return result;
	}

	public static DinamicData getDinamicValueOfData(String data) {
		DinamicData dinamicData = new DinamicData();
		String lenghtOfDataStr = data.substring(0, PosManagerConstantsAndEnums.CHARACTER_TO_READ_LENGHT_NEXT_FIELD);
		int lenght = Integer.parseInt(lenghtOfDataStr, 16);
		dinamicData.setLenght(lenght);
		dinamicData.setValue(data.substring(PosManagerConstantsAndEnums.CHARACTER_TO_READ_LENGHT_NEXT_FIELD,
				lenght + PosManagerConstantsAndEnums.CHARACTER_TO_READ_LENGHT_NEXT_FIELD));
		dinamicData.setData(data.substring(lenght + PosManagerConstantsAndEnums.CHARACTER_TO_READ_LENGHT_NEXT_FIELD));
		return dinamicData;
	}

	private static String getBirthDateByCf(String cf) {
		StringBuilder date = new StringBuilder(cf.substring(PosManagerConstantsAndEnums.CHARACTER_TO_START_BIRTHDATE,
				PosManagerConstantsAndEnums.CHARACTER_TO_END_BIRTHDATE));
		StringBuilder year = new StringBuilder(date.substring(PosManagerConstantsAndEnums.CHARACTER_TO_START_YEAR,
				PosManagerConstantsAndEnums.CHARACTER_TO_END_YEAR));
		StringBuilder month = new StringBuilder(date.substring(PosManagerConstantsAndEnums.CHARACTER_TO_START_MONTH,
				PosManagerConstantsAndEnums.CHARACTER_TO_END_MONTH));
		StringBuilder day = new StringBuilder(
				(date.substring(PosManagerConstantsAndEnums.CHARACTER_TO_START_DAY, date.length())));

		Calendar c = new GregorianCalendar();
		int annoForControl = Integer.parseInt(year.toString()) + PosManagerConstantsAndEnums.YEAR_TO_SUM;
		int toDayYear = c.get(Calendar.YEAR);
		int dayForControl = Integer.parseInt(day.toString());

		addPrefixYear(year, annoForControl, toDayYear);
		month = new StringBuilder(CfMonth.valueOf(month.toString()).getMonth());
		if (dayForControl > 31) {
			day = new StringBuilder();
			int womanDay = dayForControl - PosManagerConstantsAndEnums.WOMAN_DAY;
			day.append(womanDay);
		}
		date = new StringBuilder();
		date.append(day);
		date.append("/");
		date.append(month);
		date.append("/");
		date.append(year);
		return date.toString();
	}

	private static void addPrefixYear(StringBuilder anno, int annoForControl, int toDayYear) {
		if (annoForControl < toDayYear) {
			int anni = toDayYear - annoForControl;
			anno.insert(0, PosManagerConstantsAndEnums.YEAR_ADDING_FOR_OLD_MAN);
			if (anni >= PosManagerConstantsAndEnums.YEAR_OLD_FOR_OPERATION) {
				anno.insert(0, PosManagerConstantsAndEnums.YEAR_ADDING_FOR_YOUNG_MAN);
			}
		} else {
			anno.insert(0, PosManagerConstantsAndEnums.YEAR_ADDING_FOR_OLD_MAN);
		}
	}

	public static String fillZeroSx(int fill, String value) {
		StringBuilder fillValue = new StringBuilder(value);
		for (int i = value.length(); i < fill; i++) {
			fillValue.insert(0, "0");
		}
		return fillValue.toString();
	}
	
	public static String fillZeroDx(int fill, String value) {
		StringBuilder fillValue = new StringBuilder(value);
		for (int i = value.length(); i < fill; i++) {
			fillValue.append("0");
		}
		return fillValue.toString();
	}

	public static String getEndValidityDateFromOperatorCard(String cardData) throws ParseException {
		int[] yearIndexs = { 79, 80, 77, 78 };
		int[] monthIndexs = { 73, 74 };
		int[] dayIndexs = { 75, 76 };
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");

		StringBuilder dayHex = new StringBuilder();
		StringBuilder monthHex = new StringBuilder();
		StringBuilder yearHex = new StringBuilder();
		StringBuilder expiredDate = new StringBuilder();
		for (int i : dayIndexs) {
			dayHex.append(cardData.substring(i - 1, i));
		}
		for (int i : monthIndexs) {
			monthHex.append(cardData.substring(i - 1, i));
		}
		for (int i : yearIndexs) {
			yearHex.append(cardData.substring(i - 1, i));
		}
		log.debug("Day hex:" + dayHex);
		log.debug("Month hex:" + monthHex);
		log.debug("Year hex:" + yearHex);
		StringBuilder day = new StringBuilder();
		day.append(Integer.parseUnsignedInt(dayHex.toString(), 16));
		day = new StringBuilder(fillZeroSx(2, day.toString()));
		StringBuilder month = new StringBuilder();
		month.append(Integer.parseUnsignedInt(monthHex.toString(), 16));
		month = new StringBuilder(fillZeroSx(2, month.toString()));
		StringBuilder year = new StringBuilder();
		year.append(Integer.parseUnsignedInt(yearHex.toString(), 16));
		log.debug("Day:" + day);
		log.debug("Month:" + month);
		log.debug("Year:" + year);
		expiredDate.append(day);
		expiredDate.append(month);
		expiredDate.append(year);
		log.debug("ExpirationDate: " + expiredDate);
		long date = sdf.parse(expiredDate.toString()).getTime();
		log.debug("ExpirationDate in millis: " + date);
		return String.valueOf(date);
	}

	public static String getPinDateFromOperatorCard(String cardData) throws ParseException {
		int[] pinIndexs = { 63, 64, 61, 62 };
		StringBuilder pinHex = new StringBuilder();
		for (int i : pinIndexs) {
			pinHex.append(cardData.substring(i - 1, i));
		}
		log.debug("Pin hex:" + pinHex);
		StringBuilder pin = new StringBuilder();
		pin.append(Integer.parseUnsignedInt(pinHex.toString(), 16));
		pin = new StringBuilder(pin.toString());
		log.debug("Pin:" + pin);
		return String.valueOf(pin);
	}

	private static String formatBirthDate(String birthDate) {
		StringBuilder birth = new StringBuilder();
		birth.append(birthDate.substring(0, 2));
		birth.append("/");
		birth.append(birthDate.substring(2, 4));
		birth.append("/");
		birth.append(birthDate.substring(4));
		return birth.toString();
	}
	
	public static Map<String, Object> propertiesFromConfigList(Map<String, Config> configList) {
		LinkedHashMap<String, Object> propertiesMap = new LinkedHashMap<>();
		for (Map.Entry<String, Config> entry : configList.entrySet()) {
			Config config = entry.getValue();

			log.debug("propertiesFromConfigList >> name:{}, type:{}, value:{}, structValue:{}", 
					config.getName(),TypeEnum.getByValue(config.getType()), config.getValue(),  config.getStructuredValue()) ;
			switch (TypeEnum.getByValue(config.getType())) {
			case INTEGER:
				propertiesMap.put(config.getName(), Integer.parseInt(config.getValue()));
				break;
			case BOOLEAN:
				propertiesMap.put(config.getName(), Boolean.parseBoolean(config.getValue()));
				break;
			case STRUCT:
				propertiesMap.put(config.getName(), config.getStructuredValue());
				break;
			case STRING:
			default:
				propertiesMap.put(config.getName(), config.getValue());
				break;
			}
		}
		return propertiesMap;
	}

}
