
package posPackage;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InstalledApplicationInfo {

    @SerializedName("InstalledApplications")
    @Expose
    private InstalledApplicationDetails installedApplicationdetails;

    public InstalledApplicationDetails getInstalledApplicationInfo() {
        return installedApplicationdetails;
    }

    public void setInstalledApplicationList(InstalledApplicationDetails installedApplicationdetail) {
        this.installedApplicationdetails = installedApplicationdetail;
    }

}
