package posPackage;

public enum Location {
	MILAN("MI"),
	ROME("RO"),
	UNKNOWN("unknown");
	
	private String value;
	
	private Location(String value) {
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
}
