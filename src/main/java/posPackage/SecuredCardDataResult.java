package posPackage;

public class SecuredCardDataResult {

	public enum Result {
		OK("Card data read successfully"),
		MASK_ERROR("PAN mask invalid format or too few hidden characters");

		private final String description;

		Result(final String description) {
			this.description = description;
		}

		@Override
		public String toString() {
			return this.description;
		}    
	}

	private Result result;
	private String maskedPan;
	private String encryptedPan;
	private String keyIndexRSA;
	
	public SecuredCardDataResult() {
		keyIndexRSA = "0";
	}

	public void setResult(Result result) {
		this.result = result;
	}

	public void setMaskedPan(String maskedPan) {
		this.maskedPan = maskedPan;
	}

	public void setEncryptedPan(String encryptedPan) {
		this.encryptedPan = encryptedPan;
	}

	public Result getResult() {
		return this.result;
	}

	public String getMaskedPan() {
		return this.maskedPan;
	}

	public String getEncryptedPan() {
		return this.encryptedPan;
	}

	public String getKeyIndexRSA() {
		return keyIndexRSA;
	}

}
