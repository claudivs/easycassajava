package posPackage;

public class DeviceTestResult {
    
    public enum Result {
       OK("Reconciliation transaction performed successfully"),
       ERROR("Reconciliation transaction error");
    
       private final String description;

       Result(final String description) {
           this.description = description;
       }

       @Override
       public String toString() {
           return this.description;
       }    
    }
        
    private Result result;
        
    public void setResult(Result result) {
		this.result = result;
	}

	public Result getResult() {
        return this.result;
    }
}

