package posPackage;



import it.netsw.apps.ftfs.posdealer.impl.ReconciliationData;
import it.netsw.apps.ftfs.posdealer.impl.TransactionData;
import it.netsw.apps.ftfs.sisal.PosSisal;
import it.netsw.coms.device.IDevice;
import it.netsw.coms.device.ingenico.pos.comms.PaymentType;

public class PosSisalWrapper {

	private PosSisal pos;
	
	public PosSisalWrapper(IDevice iDevice) throws Exception {
		pos = new PosSisal(iDevice);
	}
	
	public TransactionData doStatus() {
		return pos.doStatus();
	}

	public void setDebug(int i) {
		pos.setDebug(i);
	}

	public void setLogger(SisalPosLogger logger) {
		pos.setLogger(logger);
	}

	public TransactionData doSecureField(String cb2Cid, int i, String panMask, int j, int k) {
		return null;
	}

	public TransactionData doSLE442(String cb2Cid, int i, int j, String string, String string2) {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean doReprintFinReceipt(boolean b) {
		// TODO Auto-generated method stub
		return false;
	}

	public String getLastTrReceipt() {
		// TODO Auto-generated method stub
		return null;
	}

	public String dolibVersion() {
		// TODO Auto-generated method stub
		return null;
	}

	public ReconciliationData doReconciliation(boolean cb2PrintOnPos, boolean cb2ReadReceipt) {
		// TODO Auto-generated method stub
		return null;
	}

	public TransactionData doRSAKey(String rsaCid, int i, String string, String string2) {
		// TODO Auto-generated method stub
		return null;
	}

	public TransactionData doConfCb2(String cb2Cid, String tid, String cb2CompanyCode, String cb2Url, String string,
			int i, String cb2Slot, boolean cb2PrintOnPos, boolean cb2ReadReceipt) {
		// TODO Auto-generated method stub
		return null;
	}

	public void setPosId(String cb2PassePartout) {
		// TODO Auto-generated method stub
		
	}

	public TransactionData doPayment(String transactionIdString, String xid, String string, int amount,
			boolean cb2PrintOnPos, boolean cb2ReadReceipt, PaymentType paymentType) {
		// TODO Auto-generated method stub
		return null;
	}

	public TransactionData doCFCNS(int codeType) {
		// TODO Auto-generated method stub
		return null;
	}
	
	

}
