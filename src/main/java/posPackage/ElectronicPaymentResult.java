package posPackage;


public class ElectronicPaymentResult {

	public enum Result {
		OK("Electronic payment completed successfully"),
		PAYMENT_ERROR("Electronic payment failure");

		private final String description;

		Result(final String description) {
			this.description = description;
		}

		@Override
		public String toString() {
			return this.description;
		}    
	}

	private Result result;
	private String resultCode;
	private String receipt[];
	private String addInfo;
	private String codAuth;
	private String dateTime;
	private String acquirer;

	public Result getResult() {
		return this.result;
	}

	public String getResultCode() {
		return this.resultCode;
	}

	public String[] getReceipt() {
		return this.receipt;
	}    

	public String getAddInfo () {
		return this.addInfo;
	}

	public String getCodAuth() {
		return this.codAuth;
	}

	public String getDateTime() {
		return this.dateTime;
	}

	public String getAcquirer() {
		return this.acquirer;
	}
	
	public void setResult(Result result) {
		this.result = result;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public void setReceipt(String[] receipt) {
		this.receipt = receipt;
	}

	public void setAddInfo(String addInfo) {
		this.addInfo = addInfo;
	}

	public void setCodAuth(String codAuth) {
		this.codAuth = codAuth;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	public void setAcquirer(String acquirer) {
		this.acquirer = acquirer;
	}

}


