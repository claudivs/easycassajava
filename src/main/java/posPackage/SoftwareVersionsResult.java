package posPackage;

public class SoftwareVersionsResult {
    
    public enum Result {
       OK("Software versions data retrieved correctly"),
       ERROR("Software versions data error");
    
       private final String description;

       Result(final String description) {
           this.description = description;
       }

       @Override
       public String toString() {
           return this.description;
       }    
    }
        
    private Result result;
    private String javaLibVersion;
    private String operatingSystemVersion;
    private String managerVersion;
    private String systemSecurityApplicationVersion;
    private String masterVersion;
    private String paymentApplicationVersion;
    private String cashManagerVersion;
    private String sslManagerVersion;
    private String externalManagerVersion;
    private String ecrDllVersion;
        
    public void setResult(Result result) {
		this.result = result;
	}

	public void setJavaLibVersion(String javaLibVersion) {
		this.javaLibVersion = javaLibVersion;
	}

	public void setOperatingSystemVersion(String operatingSystemVersion) {
		this.operatingSystemVersion = operatingSystemVersion;
	}

	public void setManagerVersion(String managerVersion) {
		this.managerVersion = managerVersion;
	}

	public void setSystemSecurityApplicationVersion(String systemSecurityApplicationVersion) {
		this.systemSecurityApplicationVersion = systemSecurityApplicationVersion;
	}

	public void setMasterVersion(String masterVersion) {
		this.masterVersion = masterVersion;
	}

	public void setPaymentApplicationVersion(String paymentApplicationVersion) {
		this.paymentApplicationVersion = paymentApplicationVersion;
	}

	public void setCashManagerVersion(String cashManagerVersion) {
		this.cashManagerVersion = cashManagerVersion;
	}

	public void setSslManagerVersion(String sslManagerVersion) {
		this.sslManagerVersion = sslManagerVersion;
	}

	public void setExternalManagerVersion(String externalManagerVersion) {
		this.externalManagerVersion = externalManagerVersion;
	}

	public void setEcrDllVersion(String ecrDllVersion) {
		this.ecrDllVersion = ecrDllVersion;
	}

	public Result getResult() {
        return this.result;
    }

    public String getJavaLibVersion() {
        return this.javaLibVersion;
    }
    
    public String getOperatingSystemVersion() {
        return this.operatingSystemVersion;
    }
    
    public String getManagerVersion() {
        return this.managerVersion;
    }
    
    public String getSystemSecurityApplicationVersion() {
        return this.systemSecurityApplicationVersion;
    }
    
    public String getMasterVersion() {
        return this.masterVersion;
    }
    
    public String getPaymentApplicationVersion() {
        return this.paymentApplicationVersion;
    }
    
    public String getCashManagerVersion() {
        return this.cashManagerVersion;
    }
    
    public String getSslManagerVersion() {
        return this.sslManagerVersion;
    }
    
    public String getExternalManagerVersion() {
        return this.externalManagerVersion;
    }
    
    public String getEcrDllVersion() {
        return this.ecrDllVersion;
    }
}

