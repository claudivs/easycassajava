package posPackage;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import posPackage.Config;

public abstract class SisalConfigurableComponent {

	protected Map<String, Object> properties;
	private Map<String, Config> configMap;
	private static Logger logger = LoggerFactory.getLogger(SisalConfigurableComponent.class);

	public Map<String, Object> getProperties() {
		return properties;
	}

	protected abstract void update(Map<String, Object> properties);

	void updateConfigMap(Map<String, Config> configurations) {

		if (this.configMap == null) {
			this.configMap = new HashMap<>();
		}

		if (this.properties == null) {
			this.properties = new LinkedHashMap<>();
		}

		if (configurations != null) {
			this.configMap.putAll(configurations);
			this.properties.putAll(Utils.propertiesFromConfigList(configurations));
		}
	}

	Map<String, Config> getConfigMap() {
		return configMap;
	}

}
